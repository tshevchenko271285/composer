<?php

namespace Tshevchenko\Composer;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Log::notice('I`m hire: ' . self::class);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }
}
